var admin = require('firebase-admin');

var serviceAccount = require('./project1-d5560-firebase-adminsdk-inv6r-223f1ec752.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://<DATABASE_NAME>.firebaseio.com'
});

// This registration token comes from the client FCM SDKs.
var registrationToken = "ef9xc6jaqyk:APA91bGSJhCYHdv_0OVuhE7xmdeuAo7V9MgmgBgz46z4QCSbNL4kdmnRHQd4lXw4n4jz3iJ3G_QYUrKYVOe6n27ET6vPPWDUFfa7gXBOMRiG9B6_9JdlNgKn4MK_wz4E88CMZRyaoDZX";

// See the "Defining the message payload" section below for details
// on how to define a message payload.
var payload = {
  data: {
    score: "850",
    time: "2:45"
  }
};

// Send a message to the device corresponding to the provided
// registration token.
admin.messaging().sendToDevice(registrationToken, payload)
  .then(function(response) {
    // See the MessagingDevicesResponse reference documentation for
    // the contents of response.
    console.log("Successfully sent message:", response);
  })
  .catch(function(error) {
    console.log("Error sending message:", error);
  });